<?php

namespace Ezenit\Tipsa\Model\Source;

class Manipulation
{
    public function toOptionArray()
    {
        $arr = array();
        $arr[] = array('value'=>'N', 'label'=>'No');
        $arr[] = array('value'=>'F', 'label'=>'Fijo');
        $arr[] = array('value'=>'V', 'label'=>'Variable');
        return $arr;
    }

    public function getMethod($shippingMethod)
    {
      $mte= explode("_",$shippingMethod);
      if ($mte[0]=="tipsa")
      {
        $label=$mte[1];
        foreach($this->toOptionArray() as $mta)
        {
          if ($mta["label"]==$label) $method=$mta;
        }
      }
      return $method;
    }
}
?>
