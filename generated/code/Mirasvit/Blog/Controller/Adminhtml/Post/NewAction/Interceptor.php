<?php
namespace Mirasvit\Blog\Controller\Adminhtml\Post\NewAction;

/**
 * Interceptor class for @see \Mirasvit\Blog\Controller\Adminhtml\Post\NewAction
 */
class Interceptor extends \Mirasvit\Blog\Controller\Adminhtml\Post\NewAction implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Mirasvit\Blog\Api\Repository\PostRepositoryInterface $postRepository, \Magento\Framework\Registry $registry, \Magento\Backend\App\Action\Context $context)
    {
        $this->___init();
        parent::__construct($postRepository, $registry, $context);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
