var config =
{
    config:
    {
        mixins:
        {
            'Magento_Checkout/js/view/shipping':
            {
                'Ezenit_Tipsa/js/view/shipping': true
            }
        }
    },
    map:{
      "*": {
        "googlemaps": "Ezenit_Tipsa/js/googlemaps"
      }
    }
};
