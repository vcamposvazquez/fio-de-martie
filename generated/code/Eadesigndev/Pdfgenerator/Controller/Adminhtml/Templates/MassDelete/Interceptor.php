<?php
namespace Eadesigndev\Pdfgenerator\Controller\Adminhtml\Templates\MassDelete;

/**
 * Interceptor class for @see \Eadesigndev\Pdfgenerator\Controller\Adminhtml\Templates\MassDelete
 */
class Interceptor extends \Eadesigndev\Pdfgenerator\Controller\Adminhtml\Templates\MassDelete implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Ui\Component\MassAction\Filter $filter, \Eadesigndev\Pdfgenerator\Model\ResourceModel\Pdfgenerator\CollectionFactory $templateCollectionFactory)
    {
        $this->___init();
        parent::__construct($context, $filter, $templateCollectionFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
