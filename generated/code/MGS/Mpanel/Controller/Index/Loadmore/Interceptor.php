<?php
namespace MGS\Mpanel\Controller\Index\Loadmore;

/**
 * Interceptor class for @see \MGS\Mpanel\Controller\Index\Loadmore
 */
class Interceptor extends \MGS\Mpanel\Controller\Index\Loadmore implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Element\Context $urlContext)
    {
        $this->___init();
        parent::__construct($context, $urlContext);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
