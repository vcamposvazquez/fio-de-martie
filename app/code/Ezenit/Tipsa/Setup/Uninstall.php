<?php
namespace Ezenit\Tipsa\Setup;

use Magento\Framework\Setup\UninstallInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class Uninstall implements UninstallInterface
{
    public function uninstall(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $table = $setup->getConnection()->dropTable('ezenit_tipsa_tablerates');
        $table = $setup->getConnection()->dropTable('ezenit_tipsa_yupick_orders');

        $setup->endSetup();
    }
}
