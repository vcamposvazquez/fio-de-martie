<?php
namespace Sunarc\Visualcatalog\Controller\Adminhtml\Catalog\Save;

/**
 * Interceptor class for @see \Sunarc\Visualcatalog\Controller\Adminhtml\Catalog\Save
 */
class Interceptor extends \Sunarc\Visualcatalog\Controller\Adminhtml\Catalog\Save implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Sales\Model\OrderFactory $orderFactory, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Catalog\Model\CategoryFactory $categoryFactory)
    {
        $this->___init();
        parent::__construct($context, $orderFactory, $resultJsonFactory, $resultPageFactory, $categoryFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
