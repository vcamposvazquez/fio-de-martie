/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*global define*/
define(
    [
        'jquery',
        'underscore',
        'Magento_Ui/js/form/form',
        'ko',
        'Magento_Customer/js/model/customer',
        'Magento_Customer/js/model/address-list',
        'Magento_Checkout/js/model/address-converter',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/action/create-shipping-address',
        'Magento_Checkout/js/action/select-shipping-address',
        'Magento_Checkout/js/model/shipping-rates-validator',
        'Magento_Checkout/js/model/shipping-address/form-popup-state',
        'Magento_Checkout/js/model/shipping-service',
        'Magento_Checkout/js/action/select-shipping-method',
        'Magento_Checkout/js/model/shipping-rate-registry',
        'Magento_Checkout/js/action/set-shipping-information',
        'Magento_Checkout/js/model/step-navigator',
        'Magento_Ui/js/modal/modal',
        'Magento_Checkout/js/model/checkout-data-resolver',
        'Magento_Checkout/js/checkout-data',
        'uiRegistry',
        'mage/translate',
        'Magento_Checkout/js/model/shipping-rate-service'
    ],
    function (
        $,
        _,
        Component,
        ko,
        customer,
        addressList,
        addressConverter,
        quote,
        createShippingAddress,
        selectShippingAddress,
        shippingRatesValidator,
        formPopUpState,
        shippingService,
        selectShippingMethodAction,
        rateRegistry,
        setShippingInformationAction,
        stepNavigator,
        modal,
        checkoutDataResolver,
        checkoutData,
        registry,
        $t
    ) {
        'use strict';
        var ezEraser = {
          deleteAsociation: function(e){
            jQuery.ajax({
              url: window.checkoutConfig.ezenit.urls.delete,
              type: "get",
              data: {
                quote_id: window.checkoutConfig.ezenit.quote,
                tms: (+ new Date())
              },
              success: function(response) {
                return true;
              },
              error: function(xhr) {
                console.log("Error while deleting the yupick asociation");
              }
            });
          }
        };
        var ezListener = {
          onSelectedSpot: function(e){
            var $button = jQuery(e.currentTarget);
            jQuery.ajax({
              url: window.checkoutConfig.ezenit.urls.save,
              type: "get",
              data: {
                quote_id: window.checkoutConfig.ezenit.quote,
                yupick_id: $button.data('id'),
                name: $button.data('name'),
                country: $button.data('country'),
                address: $button.data('address'),
                city: $button.data('city'),
                postal_code: $button.data('postalcode'),
                tms: (+ new Date())
              },
              success: function(response) {
                jQuery('.ez-yupick-errors').html('<span class="yupick-msg">Seleccionado: ' + $button.data('name') + '</span>');
              },
              error: function(xhr) {
                console.log("Error while retrieving data from Yupick. Contact with the administrators.");
              }
            });
          }
        };
        var ezCleaner = {
          cleanMap: function(){
            if (window.checkoutConfig.ezenit.ezMarkers.length) {
              jQuery.each(window.checkoutConfig.ezenit.ezMarkers, function(){
                this.setMap(null);
              })
            }
            window.checkoutConfig.ezenit.ezMarkers = [];
            window.checkoutConfig.ezenit.ezInfoWindows = [];
          }
        };
        var ezSpots = {
          placeSpots: function(spots){
            ezCleaner.cleanMap();
            var midLat = 0;
            var midLng = 0;
            var lat_min = parseFloat(spots[0].lat);
            var lat_max = parseFloat(spots[0].lat);
            var lng_min = parseFloat(spots[0].lng);
            var lng_max = parseFloat(spots[0].lng);
            jQuery.each(spots, function(e){
              midLat = midLat + parseFloat(this.lat);
              midLng = midLng + parseFloat(this.lng);
              if (lat_min > parseFloat(this.lat)) {lat_min = parseFloat(this.lat);}
              if (lat_max < parseFloat(this.lat)) {lat_max = parseFloat(this.lat);}
              if (lng_min > parseFloat(this.lng)) {lng_min = parseFloat(this.lng);}
              if (lng_max < parseFloat(this.lng)) {lng_max = parseFloat(this.lng);}
              ezSpot.placeSpot(this);
            })
            window.checkoutConfig.ezenit.ezMap.setCenter(new google.maps.LatLng(
              ((midLat) / parseInt(spots.length)),
              ((midLng) / parseInt(spots.length))
            ));
            window.checkoutConfig.ezenit.ezMap.fitBounds(new google.maps.LatLngBounds(
              new google.maps.LatLng(lat_min, lng_min),
              new google.maps.LatLng(lat_max, lng_max)
            ));
          }
        };
        var ezSpot = {
          placeSpot: function(spot){
            var image = {
                        url: window.checkoutConfig.ezenit.logo,
                        scaledSize: new google.maps.Size(42, 60),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(21, 30)
                      };
            var marker = new google.maps.Marker({
                position: {lat: parseFloat(spot.lat), lng: parseFloat(spot.lng)},
                animation: google.maps.Animation.DROP,
                title: spot.name,
                icon: image
            });
            var infowindow = new google.maps.InfoWindow();
            window.checkoutConfig.ezenit.ezInfoWindows.push(infowindow);
            var content    = '<div class="infowindow">' +
                             '<span class="ez-spot-field">' + spot.name + '</span><br/>' +
                             '<span class="ez-spot-field">' + spot.address + '</span><br/>' +
                             '<span class="ez-spot-field">' + spot.city + ' - ' + spot.postalcode + '</span><br/>' +
                             '<span class="ez-spot-field">Parking: ' + spot.parking + ' - Wifi: ' + spot.wifi + '</span><br/>' +
                             '<span class="ez-spot-field">Prensa: ' + spot.press + ' - Alimentación: ' + spot.food + '</span><br/>' +
                             '<span class="ez-spot-field">Tarjeta Crédito: ' + spot.creditcard + '</span><br/>' +
                             '<span class="ez-spot-field ez-schedule"Horario</span><br/>' +
                             '<span class="ez-spot-field">L: M:' + (typeof(spot.schedule.lunes.manana) == "object" ? 'Cerrado' : spot.schedule.lunes.manana ) + ' T: ' + (typeof(spot.schedule.lunes.tarde) == "object" ? 'Cerrado' : spot.schedule.lunes.tarde ) + '</span><br/>' +
                             '<span class="ez-spot-field">M: M:' + (typeof(spot.schedule.martes.manana) == "object" ? 'Cerrado' : spot.schedule.martes.manana ) + ' T: ' + (typeof(spot.schedule.martes.tarde) == "object" ? 'Cerrado' : spot.schedule.martes.tarde ) + '</span><br/>' +
                             '<span class="ez-spot-field">X: M:' + (typeof(spot.schedule.miercoles.manana) == "object" ? 'Cerrado' : spot.schedule.miercoles.manana ) + ' T: ' + (typeof(spot.schedule.miercoles.tarde) == "object" ? 'Cerrado' : spot.schedule.miercoles.tarde ) + '</span><br/>' +
                             '<span class="ez-spot-field">J: M:' + (typeof(spot.schedule.jueves.manana) == "object" ? 'Cerrado' : spot.schedule.jueves.manana ) + ' T: ' + (typeof(spot.schedule.jueves.tarde) == "object" ? 'Cerrado' : spot.schedule.jueves.tarde ) + '</span><br/>' +
                             '<span class="ez-spot-field">V: M:' + (typeof(spot.schedule.viernes.manana) == "object" ? 'Cerrado' : spot.schedule.viernes.manana ) + ' T: ' + (typeof(spot.schedule.viernes.tarde) == "object" ? 'Cerrado' : spot.schedule.viernes.tarde ) + '</span><br/>' +
                             '<span class="ez-spot-field">S: M:' + (typeof(spot.schedule.sabado.manana) == "object" ? 'Cerrado' : spot.schedule.sabado.manana ) + ' T: ' + (typeof(spot.schedule.sabado.tarde) == "object" ? 'Cerrado' : spot.schedule.sabado.tarde ) + '</span><br/>' +
                             '<span class="ez-spot-field">D: M:' + (typeof(spot.schedule.domingo.manana) == "object" ? 'Cerrado' : spot.schedule.domingo.manana ) + ' T: ' + (typeof(spot.schedule.domingo.tarde) == "object" ? 'Cerrado' : spot.schedule.domingo.tarde ) + '</span><br/>' +
                             '<button class="ez-select-yupick" data-name="'+spot.name+'" data-address="'+spot.address+'" data-postalcode="'+spot.postalcode+'" data-city="'+spot.city+'" data data-id="' + spot.id + '">Seleccionar</button>' +
                             '</div>';
            google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){
              return function() {
                window.checkoutConfig.ezenit.ezMap.setCenter(marker.getPosition());
                infowindow.setContent(content);
                if (window.checkoutConfig.ezenit.ezInfoWindows) {jQuery.each(window.checkoutConfig.ezenit.ezInfoWindows, function(){this.close();})}
                infowindow.open(window.checkoutConfig.ezenit.ezMap,marker);
              };
            })(marker,content,infowindow));
            window.checkoutConfig.ezenit.ezMarkers.push(marker);
            marker.setMap(window.checkoutConfig.ezenit.ezMap);
          }
        };

        shippingService.getShippingRates().subscribe(function(){
          if (ezSelectedMethod != null) {
              mixin.selectShippingMethod(ezSelectedMethod);
          }
        });

        jQuery(document).on('click','.ez-select-yupick', function(e){
          e.preventDefault();
          jQuery('#shipping-method-buttons-container').show();
          jQuery('#shipping-method-buttons-container button').trigger('click');
        })


        var ezSelectedMethod = null;

        var mixin = {
          selectShippingMethod: function (shippingMethod) {
              ezSelectedMethod = shippingMethod;
              var method_code  = shippingMethod.method_code;

              if (method_code == 'tipsa_50' || method_code == "tipsa_57") {
                jQuery('.ez-yupick-errors').css('display','block');
                jQuery('#shipping-method-buttons-container').hide();
                jQuery('#yupick-map').css('display','block').css('height','500px');

                if (jQuery('#yupick-map').children().length == 0) {
                  jQuery(document).on('click','.ez-select-yupick', ezListener.onSelectedSpot);
                  jQuery(window).bind('gMapsLoaded', function(){
                    window.checkoutConfig.ezenit.ezMap = new google.maps.Map(document.getElementById('yupick-map'), {
                      center: {lat: 40.5145302, lng:-3.567714},
                      zoom: 6
                    });
                  });
                  window.loadGoogleMaps(window.checkoutConfig.ezenit.apikey);
                }

                var pCode = jQuery('[name="postcode"]').val(); // "" if User is already logged in
                // IF NULL pCode THEN WE GET THE CUSTOMER ADDRESS
                if (pCode == "") {
                  pCode = window.checkoutConfig.customerData.addresses[0].postcode;
                }

                jQuery.ajax({
                  url: window.checkoutConfig.ezenit.urls.spots,
                  type: "get",
                  data: {
                    postalCode: pCode,
                    tms:        (+ new Date())
                  },
                  success: function(response) {
                    if (response == "NO RESULTS") {
                      jQuery('.ez-yupick-errors').html('<span class="yupick-error">No hay puntos disponibles</span>');
                      ezCleaner.cleanMap();
                    } else {
                      jQuery('.ez-yupick-errors').html('<span class="yupick-msg">Escoge un punto de recogida</span>');
                      ezSpots.placeSpots(JSON.parse(response));
                    }
                  },
                  error: function(xhr) {
                    console.log("Error while retrieving data from Yupick. Contact with the administrators!");
                  }
                });
              } else {
                jQuery('.ez-yupick-errors').css('display','none');
                jQuery('#shipping-method-buttons-container').show();
                jQuery('#yupick-map').css('display','none').css('height','0px');
                ezEraser.deleteAsociation();
              }
              selectShippingMethodAction(shippingMethod);
              checkoutData.setSelectedShippingRate(shippingMethod.carrier_code + '_' + shippingMethod.method_code);
              return true;
          }
        };
        return function (target) {
          return target.extend(mixin);
        };
    }
);
