<?php
namespace Codeko\Redsys\Controller\Index\Success;

/**
 * Interceptor class for @see \Codeko\Redsys\Controller\Index\Success
 */
class Interceptor extends \Codeko\Redsys\Controller\Index\Success implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Sales\Model\OrderFactory $order_factory, \Magento\Framework\App\ObjectManagerFactory $object_factory, \Magento\Store\Model\StoreManagerInterface $store_manager, \Magento\Sales\Model\Service\InvoiceService $invoice_service, \Magento\Framework\DB\Transaction $transaction, \Magento\Sales\Model\Order\Email\Sender\InvoiceSender $invoice_sender, \Magento\Sales\Model\OrderRepository $order_repository, \Magento\Sales\Model\Order\InvoiceRepository $invoice_repository, \Magento\Quote\Api\CartRepositoryInterface $quote_repository, \Magento\Quote\Model\QuoteFactory $quote_factory, \Magento\Sales\Api\Data\TransactionSearchResultInterfaceFactory $trans_search)
    {
        $this->___init();
        parent::__construct($context, $order_factory, $object_factory, $store_manager, $invoice_service, $transaction, $invoice_sender, $order_repository, $invoice_repository, $quote_repository, $quote_factory, $trans_search);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
