<?php
namespace Ezenit\Tipsa\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper{

  const CONFIG_URL_PATH          = 'ezenit_tipsa_settings/file_import/csv_url';
  const TABLE_NAME               = 'fdm_ezenit_tipsa_tablerates';
  const TABLE_YUPICK_NAME        = 'fdm_ezenit_tipsa_yupick_orders';

  protected $scope_config;
  protected $db_resource;
  protected $connection;
  protected $_order;
  protected $orderRepository;
  protected $orderItemRepository;
  protected $messageManager;
  protected $tipsa_logger;
  protected $_layout;



  public function __construct(
      \Magento\Framework\App\Helper\Context $context,
      \Magento\Framework\App\ResourceConnection $resource,
      \Magento\Sales\Model\OrderRepository $orderRepository,
      \Magento\Sales\Model\Order\ItemRepository $orderItemRepository,
      \Magento\Framework\Message\ManagerInterface $messageManager,
      \Ezenit\Tipsa\Logger\Logger $logger,
      \Magento\Framework\View\LayoutInterface $layout
  ){
    $this->scope_config         = $context->getScopeConfig();
    $this->db_resource          = $resource;
    $this->orderRepository      = $orderRepository;
    $this->_orderItemRepository = $orderItemRepository;
    $this->messageManager       = $messageManager;
    $this->tipsa_logger         = $logger;
    $this->_layout              = $layout;


    return parent::__construct($context);

  }

  public function getConfig($path) { return $this->scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);}
  public function getFileUrl()     { return $this->getConfig(self::CONFIG_URL_PATH);}
  public function getFilePath()    { return $this->getConfig('ezenit_tipsa_settings/file_import/xls_upload');}
  protected function getYupickUrl(){ return $this->getConfig('ezenit_tipsa_settings/ez_tipsa_yupick_settings/tipsa_yupick_url');}

  protected function getConnection()
  {
      if (!$this->connection) {
          $this->connection = $this->db_resource->getConnection('core_write');
      }
      return $this->connection;
  }

  public function tipsaLogger($message)
  {
		$this->tipsa_logger->info($message);
  }

  public function getApiKey() {return $this->getConfig('ezenit_tipsa_settings/ez_tipsa_yupick_settings/tipsa_map_api_key');}
  public function getYupickKey() {return $this->getConfig('ezenit_tipsa_settings/ez_tipsa_yupick_settings/tipsa_yupick_key');}

  public function logMessageToAdmin($messageType,$message)
  {
    if ($messageType == 'error') {
      $this->messageManager->addErrorMessage(__($message));
    } else if ($messageType == 'success') {
      $this->messageManager->addSuccessMessage(__($message));
    } else {
      $this->messageManager->addWarningMessage(__($message));
    }
  }

  public function OrderItem($orderId)
  {
    if ($orderId) {
      $orderItems = $this->_orderItemRepository->get($orderId);
    }
    return $orderItems;
  }

  public function Order($orderId)
  {
    if($orderId){
     $order = $this->orderRepository->get($orderId);
    }
     return $order;
  }

  private function getYupickAddress($quote_id)
  {
    $connection    = $this->getConnection();
    $tbl_name      = $connection->getTableName(self::TABLE_YUPICK_NAME);
    $yupickData    = $connection->fetchAll("SELECT name,address,postal_code,city,yupick_id FROM " . $tbl_name . " WHERE quote_id = '".$quote_id."'");
    if ($yupickData) {
      $result = array(
        'name'        => $yupickData[0]['name'],
        'address'     => $yupickData[0]['address'],
        'postal_code' => $yupickData[0]['postal_code'],
        'city'        => $yupickData[0]['city'],
        'yupick_id'   => $yupickData[0]['yupick_id']
      );
      return $result;
    } else {
      return false;
    }
  }

  public function addYupickOrder($quote_id,$yupick_id,$name,$address,$postal_code,$city)
  {
    $connection    = $this->getConnection();
    $tbl_name      = $connection->getTableName(self::TABLE_YUPICK_NAME);
    $_id           = (int) $connection->fetchOne("SELECT COUNT(*) FROM " . $tbl_name . " WHERE quote_id = '".$quote_id."'");
    if(!$_id){
      $sql = "INSERT INTO " . $tbl_name . "(quote_id, yupick_id, name,address,postal_code,city) VALUES ('".$quote_id."', '".$yupick_id."','".$name."','".$address."','".$postal_code."','".$city."')";
      $connection->query($sql);
    } else {
      $sql = "UPDATE " . $tbl_name . " SET name = '" . $name . "' , yupick_id = '" . $yupick_id . "' , address = '" . $address . "' , postal_code = '" . $postal_code . "' , city = '" . $city . "'  WHERE quote_id = '" . $quote_id . "'";
      $connection->query($sql);
    }
  }
  public function deleteYupickCheckout($quote_id)
  {
    $connection    = $this->getConnection();
    $tbl_name      = $connection->getTableName(self::TABLE_YUPICK_NAME);
    $_id           = (int) $connection->fetchOne("SELECT COUNT(*) FROM " . $tbl_name . " WHERE quote_id = '".$quote_id."'");
    if($_id){
      $sql = "DELETE FROM " . $tbl_name . " WHERE quote_id ='" . $quote_id . "'";
      $connection->query($sql);
    }
  }

  protected function addFare($id,$service,$country,$postal_start,$postal_end,$weight,$price,$increment)
  {
    $_connection   = $this->db_resource->getConnection();
    $tableName     = $_connection->getTableName(self::TABLE_NAME);
    $connection    = $this->getConnection();
    // $tbl_name      = $connection->getTableName(self::TABLE_NAME);
    $_id          = (int) $connection->fetchOne("SELECT COUNT(*) FROM " . $tableName . " WHERE ez_service_id = '".$id."'");
    if(!$_id){
      $sql = "INSERT INTO " . $tableName . "(ez_service_id, service,country,postal_code_starts,postal_code_ends,weight,shipping_price,increment) VALUES ('".$id."', '".$service."','".$country."','".$postal_start."','".$postal_end."','".$weight."','".$price."','".$increment."')";
      $connection->query($sql);
    } else {
      $sql = "UPDATE " . $tableName . " SET shipping_price = '" . $price . "' , increment = '" . $increment . "' WHERE ez_service_id = '" . $id . "'";
      $connection->query($sql);
    }
  }

  protected function processServices($enabledServices)
  {
    $result = "";
    foreach ($enabledServices as $key => $value) {
      if ($value == true) {
        $result = $result . "'" . str_replace("tipsa_","",$key) . "',";
      }
    }
    $result = substr_replace($result, "", -1);
    return $result;
  }

  public function getFares($country,$postal_code,$weight,$enabledServices)
  {
    $enabledServicesCount = 0;
    foreach ($enabledServices as $key => $value) {
      if ($value == true) {
        $enabledServicesCount += 1;
      }
    }
    if ($enabledServicesCount == 0) {
      $this->tipsaLogger("ERR001 TIPSA - There are no services enabled.");
      return false;
    }

    $services      = $this->processServices($enabledServices);
    $connection    = $this->getConnection();
    $tbl_name      = $connection->getTableName(self::TABLE_NAME);
    $qry           = "SELECT " . $tbl_name . ".service, "
    . $tbl_name . ".shipping_price, " . $tbl_name . ".increment FROM ( SELECT *,MIN(weight) as min_weight FROM "
    . $tbl_name . " WHERE country = '" . $country . "' AND postal_code_starts <= '" . $postal_code . "'"
    . " AND postal_code_ends >= '" . $postal_code . "' AND weight >= '" . $weight . "'"
    . " AND service IN (" . $services . ")"
    . " GROUP BY service,country,postal_code_starts,postal_code_ends) t "
    . "INNER JOIN " . $tbl_name . " ON " . $tbl_name . ".country = t.country AND "
    . $tbl_name . ".postal_code_starts = t.postal_code_starts AND "
    . $tbl_name . ".postal_code_ends = t.postal_code_ends AND "
    . $tbl_name . ".weight = t.min_weight AND "
    . $tbl_name . ".service = t.service";
    $result        = $connection->fetchAll($qry);
    return $result;
  }

  public function updateFromCsv($name)
  {
    $csvFile = file($name);
    $data = [];
    foreach ($csvFile as $line) {
        $data[] = str_getcsv($line);
        $eztest = str_getcsv($line);
    }
    $headers = array_shift($data);
    foreach ($data as $fare){
      $line = explode(";",$fare[0]);
      $service      = $line[0];
      $country      = $line[1];
      $postal_start = $line[2];
      $postal_end   = $line[3];
      $weight       = $line[4];
      $price        = $line[5];
      if (!isset($line[6])) {
        $increment    = "";
      } else {
        $increment    = $line[6];
      }
      $id           = $service . $country . $postal_start . $postal_end . $weight;
      $this->addFare($id,$service,$country,$postal_start,$postal_end,$weight,$price,$increment);
    }
  }

  public function legacyPost($url, $data) {
    $params = array(
      'http' => array(
        'method'  => 'POST',
        'content' => http_build_query($data),
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
      )
    );
    $context  = stream_context_create($params);
    $result   = file_get_contents("http:".$url, false, $context);
    if ($result === false) {
        return false;
    }

    return $result;
  }

  protected function getServerUrl()        {return $this->scopeConfig->getValue('ezenit_tipsa_settings/ez_tipsa_auth/tipsa_url', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);}
  protected function getServerServiceUrl() {return $this->scopeConfig->getValue('ezenit_tipsa_settings/ez_tipsa_auth/tipsa_url_service', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);}
  protected function getAgencyCode()       {return $this->scopeConfig->getValue('ezenit_tipsa_settings/ez_tipsa_auth/tipsa_agency_code', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);}
  protected function getUserCode()         {return $this->scopeConfig->getValue('ezenit_tipsa_settings/ez_tipsa_auth/tipsa_user_code', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);}
  protected function getUserPassword()     {return $this->scopeConfig->getValue('ezenit_tipsa_settings/ez_tipsa_auth/tipsa_client_password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);}
  protected function getPickingService()   {return $this->scopeConfig->getValue('ezenit_tipsa_settings/ez_tipsa_auth/tipsa_picking_service_active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);}


  public function loginTipsa()
  {
    $serverUrl    = $this->getServerUrl();
    $agencyCode   = $this->getAgencyCode();
    $userCode     = $this->getUserCode();
    $userPass     = $this->getUserPassword();

    $sessionId    = "";
    $errorLog     = [];

    $XML='<?xml version="1.0" encoding="utf-8"?>
          <soap:Envelope
            xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema">
              <soap:Body>
                 <LoginWSService___LoginCli>
                     <strCodAge>'.$agencyCode.'</strCodAge>
                     <strCod>'.$userCode.'</strCod>
                     <strPass>'.$userPass.'</strPass>
                 </LoginWSService___LoginCli>
              </soap:Body>
          </soap:Envelope>';


    if (!function_exists('curl_init')) {
      $postResult = $this->legacyPost($serverUrl, $data);
    } else {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_HEADER, FALSE);
      curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
      curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
      curl_setopt($ch, CURLOPT_URL, $serverUrl );
      curl_setopt($ch, CURLOPT_POSTFIELDS, $XML );
      curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
      $postResult = curl_exec($ch);
    }

    $xml = simplexml_load_string($postResult, NULL, NULL, "http://schemas.xmlsoap.org/soap/envelope/");
    $xml->registerXPathNamespace("abc","http://tempuri.org/");

    if($xml->xpath('//faultstring')){
        foreach($xml->xpath('//faultstring') as $error){
          array_push($errorLog, $error);
        }
    }
    else{
        foreach ($xml->xpath('//abc:strSesion') as $item)
        {
            $sessionId = (string) $item;
        }
    }
    $result = array(
      "session" => $sessionId,
      "errors"  => $errorLog
    );
    return $result;
  }

  protected function getOriginName()       {return $this->scopeConfig->getValue('general/store_information/name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);}
  protected function getOriginAddress()    {return $this->scopeConfig->getValue('general/store_information/street_line1', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);}
  protected function getOriginCity()       {return $this->scopeConfig->getValue('general/store_information/city', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);}
  protected function getOriginCP()         {return $this->scopeConfig->getValue('general/store_information/postcode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);}
  protected function getOriginPhone()      {return $this->scopeConfig->getValue('general/store_information/phone', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);}

  protected function getTipsaShippingData($shipment,$order,$payment,$tracks,$address,$_paymentMethod,$_trackingNumber,$packages,$_contactPerson)
  {
    $result = array(
      'order_id'        => $order->getIncrementId(),
      'shipping_method' => strtoupper(str_replace("tipsa_","",$order->getData('shipping_method'))),
      'user_code'       => $this->getUserCode(),
      'origin_name'     => $this->getOriginName(),
      'origin_address'  => $this->getOriginAddress(),
      'origin_city'     => $this->getOriginCity(),
      'origin_cp'       => $this->getOriginCP(),
      'origin_phone'    => $this->getOriginPhone(),
      'dest_name'       => $address->getName(),
      'dest_address'    => $address->getStreet()[0],
      'dest_city'       => $address->getCity(),
      'dest_cp'         => $address->getPostCode(),
      'dest_phone'      => $address->getTelephone(),
      'dest_email'      => $order->getCustomerEmail(),
      'total_packages'  => $packages,
      'weight'          => floatval($order->getWeight()),
      'contact_person'  => $_contactPerson
    );

    if ($_paymentMethod == 'cashondelivery') {
      $result['refund'] = $order->getGrandTotal();
    } else {
      $result['refund'] = '0';
    }

    if (count($_trackingNumber) == 0) {
      $result['tracking_ref'] = "";
    } else {
      $result['tracking_ref'] = $_trackingNumber[0]['number'];
    }

    return $result;
  }

  protected function getServiceSms($serviceCode) { return $this->scopeConfig->getValue('ezenit_tipsa_settings/ez_tipsa_shipping_service_tipsa_' . $serviceCode . '/tipsa_sms', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);}

  protected function sendShipment($session,$tipsaShippingData,$pickingTrack)
  {
    $serverUrl    = $this->getServerUrl();
    $serviceCode  = str_replace("tipsa_","",$tipsaShippingData['shipping_method']);
    $sms          = $this->getServiceSms($serviceCode) === false ? 1 : 0;
    $errorLog     = [];

    if ($serviceCode == "50" || $serviceCode == "57") {
      $destinationAgency = "000050";
      $_trackingRef  = $tipsaShippingData['dest_cp']. ' - ' . $tipsaShippingData['tracking_ref'];
    } else {
      $destinationAgency = "";
      $_trackingRef  = $tipsaShippingData['tracking_ref'];
    }

    if ($pickingTrack == "") {
      $labelObservations = 'Pedido: #'.$tipsaShippingData['order_id'];
    } else {
      $labelObservations = 'Picking: '. $pickingTrack;
    }

    $XML='<?xml version="1.0" encoding="utf-8"?>
        <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
                       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                       xmlns:xsd="http://www.w3.org/2001/XMLSchema">
             <soap:Header>
                 <ROClientIDHeader xmlns="http://tempuri.org/">
                     <ID>'.$session['session'].'</ID>
                 </ROClientIDHeader>
             </soap:Header>
            <soap:Body>
                <WebServService___GrabaEnvio14 xmlns="http://tempuri.org/">
                  <strCodAgeCargo>'.$tipsaShippingData['agency_code'].'</strCodAgeCargo>
                	<strCodAgeOri>'.$tipsaShippingData['agency_code'].'</strCodAgeOri>
                	<dtFecha>'.date("Y/m/d").'</dtFecha>
                	<strCodAgeDes>'.$destinationAgency.'</strCodAgeDes>
                	<strCodTipoServ>'.$serviceCode.'</strCodTipoServ>
                	<strCodCli>'.$tipsaShippingData['user_code'].'</strCodCli>
                	<strCodCliDep></strCodCliDep>
                	<strNomOri>'.$tipsaShippingData['origin_name'].'</strNomOri>
                	<strTipoViaOri></strTipoViaOri>
                	<strDirOri>'.$tipsaShippingData['origin_address'].'</strDirOri>
                	<strNumOri></strNumOri>
                	<strPisoOri></strPisoOri>
                	<strPobOri>'.$tipsaShippingData['origin_city'].'</strPobOri>
                	<strCPOri>'.$tipsaShippingData['origin_cp'].'</strCPOri>
                	<strCodProOri></strCodProOri>
                	<strTlfOri>'.$tipsaShippingData['origin_phone'].'</strTlfOri>
                	<strNomDes>'.$tipsaShippingData['dest_name'].'</strNomDes>
                	<strTipoViaDes></strTipoViaDes>
                	<strDirDes>'.$tipsaShippingData['dest_address'].'</strDirDes>
                	<strNumDes></strNumDes>
                	<strPisoDes></strPisoDes>
                	<strPobDes>'.$tipsaShippingData['dest_city'].'</strPobDes>
                	<strCPDes>'.$tipsaShippingData['dest_cp'].'</strCPDes>
                	<strCodProDes></strCodProDes>
                	<strTlfDes>'.$tipsaShippingData['dest_phone'].'</strTlfDes>
                	<intDoc>0</intDoc>
                	<intPaq>'.floatval($tipsaShippingData['total_packages']).'</intPaq>
                	<dPesoOri>'.$tipsaShippingData['weight'].'</dPesoOri>
                	<dAltoOri>0.0</dAltoOri>
                	<dAnchoOri>0.0</dAnchoOri>
                	<dLargoOri>0.0</dLargoOri>
                	<dReembolso>'.$tipsaShippingData['refund'].'</dReembolso>
                	<dValor>0.0</dValor>
                	<dAnticipo>0.0</dAnticipo>
                	<dCobCli>0.0</dCobCli>
                	<strObs>'.$labelObservations.'</strObs>
                	<boSabado>false</boSabado>
                	<boRetorno>false</boRetorno>
                	<boGestOri>false</boGestOri>
                	<boGestDes>false</boGestDes>
                	<boAnulado>false</boAnulado>
                	<boAcuse>false</boAcuse>
                	<strRef>'.$_trackingRef.'</strRef>
                	<strCodSalRuta></strCodSalRuta>
                	<dBaseImp>0.0</dBaseImp>
                	<dImpuesto>0.0</dImpuesto>
                	<boPorteDebCli>false</boPorteDebCli>
                	<strPersContacto>'.$tipsaShippingData['contact_person'].'</strPersContacto>
                	<boDesSMS>'.$sms.'</boDesSMS>
                	<boDesEmail>true</boDesEmail>
                	<strDesMoviles>'.$tipsaShippingData['dest_phone'].'</strDesMoviles>
                	<strDesDirEmails>'.$tipsaShippingData['dest_email'].'</strDesDirEmails>
                	<dtHoraEntrIni></dtHoraEntrIni>
                	<dtHoraEntrFin></dtHoraEntrFin>
                	<strCodPais></strCodPais>
                	<dValorFactInt>0.0</dValorFactInt>
                	<boInsert>true</boInsert>
                	<strAlbaran></strAlbaran>
                	<strConcAgru></strConcAgru>
                	<strCargosAduaneros>0.0</strCargosAduaneros>
                	<strCodDirAgrupDes></strCodDirAgrupDes>
                	<boSeguroFranquicia>false</boSeguroFranquicia>
                </WebServService___GrabaEnvio14>
            </soap:Body>
        </soap:Envelope>';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
        curl_setopt($ch, CURLOPT_URL, $serverUrl );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $XML );
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));

        $postResult = curl_exec($ch);

        if (curl_errno($ch)) {
            if ($serviceCode=="49"||$serviceCode=="24"||$serviceCode=="50"||$serviceCode=="57"){
            array_push($errorLog, "ERROR NO SE PUEDE COMUNICAR CON TIPSA");
          }
        }

        $xml = simplexml_load_string($postResult, NULL, NULL, "http://http://www.w3.org/2003/05/soap-envelope");
        $xml->registerXPathNamespace("abc","http://tempuri.org/");

        $deliveryNote='';
        $tipsaTrackingNumber='';

        if($xml->xpath('//faultstring')){
          foreach($xml->xpath('//faultstring') as $error){
            array_push($errorLog, $error);
              if ($serviceCode=="49"||$serviceCode=="24"||$serviceCode=="50"||$serviceCode=="57"){
                array_push($errorLog, $error);
              }
          }
          }else{
          foreach ($xml->xpath('//abc:strAlbaranOut') as $item)
            {
              $deliveryNote= (string) $item;
            }
            foreach ($xml->xpath('//abc:strGuidOut') as $item)
            {
              $tipsaTrackingNumber= (string) $item;
            }
        }

        $result =  array(
          'errors'                => $errorLog,
          'delivery_note'         => $deliveryNote,
          'tipsa_tracking_number' => $tipsaTrackingNumber
        );

        return $result;
  }

  protected function getTipsaLabel($sessionId,$deliveryNote)
  {
    $serverUrl = $this->getServerServiceUrl();
    $errorLog  = [];

    $XML='<?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope
    xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                <soap:Header>
                    <ROClientIDHeader xmlns="http://tempuri.org/">
                        <ID>'.$sessionId['session'].'</ID>
                    </ROClientIDHeader>
                    </soap:Header>
                <soap:Body>
                    <WebServService___ConsEtiquetaEnvio3>
                        <strAlbaran>'.$deliveryNote.'</strAlbaran>
                        <intIdRepDet>233</intIdRepDet>
                    </WebServService___ConsEtiquetaEnvio3>
                </soap:Body>
            </soap:Envelope>';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
    curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
    curl_setopt($ch, CURLOPT_URL, $serverUrl );
    curl_setopt($ch, CURLOPT_POSTFIELDS, $XML );
    curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));

    $postResult = curl_exec($ch);

    if (curl_errno($ch)) {
      array_push($errorLog, "No se puedo llamar al servicio Tipsa");
    }

    $xml = simplexml_load_string($postResult, NULL, NULL, "http://http://www.w3.org/2003/05/soap-envelope");
    $xml->registerXPathNamespace("abc","http://tempuri.org/");

    $tipsaLabel = '';

    if($xml->xpath('//faultstring')){
      foreach($xml->xpath('//faultstring') as $error){
        array_push($errorLog, $error);
      }
    }else{
      foreach ($xml->xpath('//abc:strEtiqueta') as $item)
      {
          $tipsaLabel = $item;
      }
    }

    $result = array(
      'label'     => $tipsaLabel,
      'errors'    => $errorLog
    );
    return $result;
  }

  private function is_valid_xml ($xml)
  {
    libxml_use_internal_errors(true);
    $doc = new DOMDocument('1.0', 'utf-8');
    $doc->loadXML($xml);

    $errors = libxml_get_errors();

    return empty($errors);
  }


  public function getYupickSpots($postalCode)
  {
    $errorLog   = [];
    $yupickKey  = $this->getYupickKey();
    $serverUrl  = $this->getYupickUrl();
    $XML = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
          	<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
              	<SOAP-ENV:Body>
          			<getBuscarPuntos>
          				<strClave>'.$yupickKey.'</strClave>
          				<strCodPostal>'.$postalCode.'</strCodPostal>
          			</getBuscarPuntos>
          		</SOAP-ENV:Body>
          	</SOAP-ENV:Envelope>';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
    curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
    curl_setopt($ch, CURLOPT_URL, $serverUrl );
    curl_setopt($ch, CURLOPT_POSTFIELDS, $XML );
    curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));

    $postResult = curl_exec($ch);

    if (curl_errno($ch)) {
      array_push($errorLog, "No se puedo llamar al servicio Yupick");
    }

    $xml = simplexml_load_string($postResult, NULL, NULL, "http://http://www.w3.org/2003/05/soap-envelope");
    $xml->registerXPathNamespace("abc","http://tempuri.org/");

    $result = str_replace("<?xml version='1.0' encoding='ISO-8859-1'?><puntoentrega>", "<?xml version='1.0' encoding='ISO-8859-1'?><puntos><puntoentrega>", $postResult . '</puntos>');

    $result = utf8_decode($result);

    $result = str_replace('</puntos>', '', $result);

    if (strpos($result, 'ERROR')) {
      return "NO RESULTS";
    } else {
      $dataXml = simplexml_load_string($result, 'SimpleXMLElement', LIBXML_NOCDATA);

      $arrayResult = [];

      foreach (simplexml_load_string($dataXml->xpath('//ns1:getBuscarPuntosResponse[1]/return')[0])->puntoentrega as $yupickSpot) {
        $spot = array(
          "id"              => (string) $yupickSpot->id,
          "name"            => (string) $yupickSpot->nombre[0],
          "postalcode"      => (string) $yupickSpot->codigopostal,
          "address"         => (string) $yupickSpot->direccion,
          "city"            => (string) $yupickSpot->localidad,
          "region"          => (string) $yupickSpot->provincia,
          "comment"         => (string) $yupickSpot->comentario,
          "image"           => (string) $yupickSpot->foto,
          "lat"             => (string) $yupickSpot->poslatitud,
          "lng"             => (string) $yupickSpot->poslongitud,
          "parking"         => (string) $yupickSpot->parking,
          "wifi"            => (string) $yupickSpot->wifi,
          "food"            => (string) $yupickSpot->alimentacion,
          "press"           => (string) $yupickSpot->prensarevistas,
          "creditcard"      => (string) $yupickSpot->tarjetacredito,
          "schedule"        => $yupickSpot->horario
        );
        array_push($arrayResult, $spot);
      }

      return json_encode($arrayResult);
    }
  }

  public function setShipment($shipment,$order,$payment,$tracks,$address,$packages)
  {
    $_contactPerson   = "";
    $_shippingMethod  = $order->getData('shipping_method');

    if ( strpos($_shippingMethod, 'tipsa') === false) {
      $this->messageManager->addWarningMessage(__('The shipment '. $shipment->getId() .' is not set with Tipsa'));
      $this->tipsaLogger("Attempted to send an order that wasn't set with Tipsa Carrier.");
      return false;
    }

    $_paymentMethod   = $payment->getData('method');

    if ($_paymentMethod != 'cashondelivery') {
      $_paymentMethod = 'other';
    }

    if ( $_shippingMethod == "tipsa_tipsa_50" || $_shippingMethod == "tipsa_tipsa_57" ) {
      $quote_id      = $order->getQuoteId();
      $yupickAddress = $this->getYupickAddress($quote_id);

      if ($yupickAddress === false) {
        $this->messageManager->addErrorMessage(__('Could not retrieve Yupick Local Data'));
        return false;
      }

      $_contactPerson    = $address->getName();
      $address->setFirstName($yupickAddress['name']);
      $address->setLastName("");
      $address->setStreet($yupickAddress['address']);
      $address->setPostCode($yupickAddress['yupick_id']);
      $address->setCity($yupickAddress['city']);
    }



    $_trackingNumber  = [];

    if (isset($_POST['tracking'])) {
      foreach ($_POST['tracking'] as $key) {
        array_push($_trackingNumber, $key);
      }
    }

    $session = $this->loginTipsa();

    if (count($session['errors']) > 0) {
      $this->messageManager->addErrorMessage(__('Could not login into Tipsa Web Service'));
      $this->tipsaLogger("COULD NOT LOG IN TIPSA SERVICES");
      return false;
    }

    $tipsaShippingData = $this->getTipsaShippingData($shipment,$order,$payment,$tracks,$address,$_paymentMethod,$_trackingNumber,$packages,$_contactPerson);
    $tipsaShippingData['agency_code'] = $this->getAgencyCode();

    $pickingTrack = "";

    if ((bool)$this->getPickingService() == true) {
      foreach ($order->getAllVisibleItems() as $orderItem) {
        $pickingTrack = $pickingTrack . '#: ' .  $orderItem->getSku() . '(x' . intval($orderItem->getQtyOrdered()).'),';
      }
      $pickingTrack = substr($pickingTrack, 0, -1);
    }

    $shipmentStatus = $this->sendShipment($session,$tipsaShippingData,$pickingTrack);

    if (count($shipmentStatus['errors']) > 0) {
      $this->messageManager->addErrorMessage(__('Could not register shipment into Tipsa Web Service'));
      $this->tipsaLogger("WS MESSAGE: ".$shipmentStatus['errors'][0]);
      $this->tipsaLogger("COULD NOT LOAD SHIPMENT TO TIPSA SERVICES");
      return false;
    }

    $result = array(
      'delivery_note'         => $shipmentStatus['delivery_note'],
      'tipsa_tracking_number' => $shipmentStatus['tipsa_tracking_number'],
      'agency_code'           => $this->getAgencyCode(),
      'user_code'             => $this->getUserCode()
    );

    $label = $this->getTipsaLabel($session,$shipmentStatus['delivery_note']);

    if (count($label['errors']) > 10) {
      $this->messageManager->addErrorMessage(__('Could not generate the label'));
      $this->tipsaLogger("COULD NOT GENERATE LABEL");
      return false;
    } else {
      $result['label'] = $label['label'];
    }
    $this->messageManager->addSuccessMessage(__('Shipment generated correctly with Tipsa'));
    return $result;
  }
}
