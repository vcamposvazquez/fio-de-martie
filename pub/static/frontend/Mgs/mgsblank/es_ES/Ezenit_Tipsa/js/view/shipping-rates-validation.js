/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/shipping-rates-validator',
        'Magento_Checkout/js/model/shipping-rates-validation-rules',
        'Ezenit_Tipsa/js/model/shipping-rates-validator',
        'Ezenit_Tipsa/js/model/shipping-rates-validation-rules'
    ],
    function (
        Component,
        defaultShippingRatesValidator,
        defaultShippingRatesValidationRules,
        tipsaShippingRatesValidator,
        tipsaShippingRatesValidationRules
    ) {
        'use strict';
        defaultShippingRatesValidator.registerValidator('tipsa', tipsaShippingRatesValidator);
        defaultShippingRatesValidationRules.registerRules('tipsa', tipsaShippingRatesValidationRules);
        return Component;
    }
);
