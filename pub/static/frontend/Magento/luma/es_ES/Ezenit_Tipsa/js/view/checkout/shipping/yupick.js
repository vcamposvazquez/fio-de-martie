define([
  'googlemaps',
  'uiComponent',
  'Magento_Customer/js/customer-data',
  'jquery',
  'ko',
  'underscore',
  'sidebar'


], function (googlemaps,Component) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Ezenit_Tipsa/checkout/shipping/yupick'
        }
    });
});
