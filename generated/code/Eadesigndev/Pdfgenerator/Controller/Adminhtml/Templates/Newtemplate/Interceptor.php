<?php
namespace Eadesigndev\Pdfgenerator\Controller\Adminhtml\Templates\Newtemplate;

/**
 * Interceptor class for @see \Eadesigndev\Pdfgenerator\Controller\Adminhtml\Templates\Newtemplate
 */
class Interceptor extends \Eadesigndev\Pdfgenerator\Controller\Adminhtml\Templates\Newtemplate implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Registry $coreRegistry, \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory)
    {
        $this->___init();
        parent::__construct($context, $coreRegistry, $resultForwardFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
