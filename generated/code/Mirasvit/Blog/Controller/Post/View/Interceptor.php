<?php
namespace Mirasvit\Blog\Controller\Post\View;

/**
 * Interceptor class for @see \Mirasvit\Blog\Controller\Post\View
 */
class Interceptor extends \Mirasvit\Blog\Controller\Post\View implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Store\Model\StoreManagerInterface $storeManager, \Mirasvit\Blog\Model\PostFactory $authorFactory, \Magento\Framework\Registry $registry, \Magento\Framework\App\Action\Context $context)
    {
        $this->___init();
        parent::__construct($storeManager, $authorFactory, $registry, $context);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
