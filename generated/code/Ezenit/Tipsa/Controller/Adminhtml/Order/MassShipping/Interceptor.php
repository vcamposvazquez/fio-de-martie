<?php
namespace Ezenit\Tipsa\Controller\Adminhtml\Order\MassShipping;

/**
 * Interceptor class for @see \Ezenit\Tipsa\Controller\Adminhtml\Order\MassShipping
 */
class Interceptor extends \Ezenit\Tipsa\Controller\Adminhtml\Order\MassShipping implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Ui\Component\MassAction\Filter $filter, \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory, \Psr\Log\LoggerInterface $logger, \Ezenit\Tipsa\Helper\Data $helper, \Magento\Sales\Model\Convert\OrderFactory $convertOrderFactory, \Magento\Sales\Model\Order\ShipmentFactory $shipmentFactory)
    {
        $this->___init();
        parent::__construct($context, $filter, $collectionFactory, $logger, $helper, $convertOrderFactory, $shipmentFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
