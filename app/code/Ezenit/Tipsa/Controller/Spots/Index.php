<?php
namespace Ezenit\Tipsa\Controller\Spots;

class Index extends \Magento\Framework\App\Action\Action
{
     /**
      * @var \Magento\Framework\Translate\Inline\StateInterface
      */
     protected $inlineTranslation;

     /**
      * @var \Magento\Framework\App\Config\ScopeConfigInterface
      */
     protected $scopeConfig;
     protected $helper;

     public function __construct(
         \Magento\Framework\App\Action\Context $context,
         \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
         \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
         \Ezenit\Tipsa\Helper\Data $helper
     ) {
         parent::__construct($context);
         $this->inlineTranslation = $inlineTranslation;
         $this->scopeConfig       = $scopeConfig;
         $this->helper            = $helper;
    }

    public function execute()
    {
        $postalCode = $_REQUEST['postalCode'];
        $result     = $this->helper->getYupickSpots($postalCode);
        echo $result;
    }
}
