<?php
namespace Ezenit\Tipsa\Controller\Delete;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * Show Sample Module main page
     *
     * @return void
     */
    protected $helper;

    public function __construct(
         \Magento\Framework\App\Action\Context $context,
         \Ezenit\Tipsa\Helper\Data $helper
     ) {
         parent::__construct($context);
         $this->helper = $helper;
    }
    public function execute()
    {
      $this->helper->deleteYupickCheckout($_REQUEST['quote_id']);
      echo json_encode(array("success" => true));
    }
}
