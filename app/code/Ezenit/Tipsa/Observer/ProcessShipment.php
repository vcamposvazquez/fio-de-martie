<?php
namespace Ezenit\Tipsa\Observer;

use Magento\Framework\Event\ObserverInterface;

class ProcessShipment implements ObserverInterface
{
    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    protected $helper;
    protected $logger;
    protected $trackFactory;
    protected $directoryList;
    protected $_file;
    protected $connection;
    protected $db_resource;

    public function __construct(
      \Ezenit\Tipsa\Helper\Data $helper,
      \Psr\Log\LoggerInterface $logger,
      \Magento\Sales\Model\Order\Shipment\TrackFactory $trackFactory,
      \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
      \Magento\Framework\Filesystem\Io\File $_file,
      \Magento\Framework\App\ResourceConnection $resource,
      array $data = []
      )
    {
      $this->helper         = $helper;
      $this->logger         = $logger;
      $this->trackFactory   = $trackFactory;
      $this->directoryList  = $directoryList;
      $this->_file          = $_file;
      $this->db_resource    = $resource;

    }

    protected function getConnection()
    {
      if (!$this->connection) {
          $this->connection = $this->db_resource->getConnection('core_write');
      }
      return $this->connection;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
      $shipment = $observer->getEvent()->getShipment();
      if (!is_null($shipment->getShippingLabel())) {
        return true;
      }
      $order    = $shipment->getOrder();
      $payment  = $order->getPayment();
      $tracks   = $shipment->getAllTracks();
      $address  = $order->getShippingAddress();
      $packages = 1;
      $tipsaShippingData = $this->helper->setShipment($shipment,$order,$payment,$tracks,$address,$packages);
      if ($tipsaShippingData ===  false) {
        $this->helper->tipsaLogger("THE SHIPMENT COULDN'T BE CREATED");
        return false;
      }
      $urlKey   = substr($tipsaShippingData['tipsa_tracking_number'],1,-1);
      $url = 'https://dinapaqweb.tipsa-dinapaq.com/dinapaqweb/detalle_envio_sc.php?servicio='.$urlKey.'&fecha='.date("d/m/Y");
      $data = array(
        'carrier_code' => 'custom',
        'title' => 'Tipsa Url Seg: ',
        'number' => $url,
      );

      $track = $this->trackFactory->create()->addData($data);
      $shipment->addTrack($track)->save();

      $pdfContent = base64_decode((string) $tipsaShippingData['label']);
      $pdfPath   = $this->directoryList->getPath('tmp').'/tipsa/labels/';
      $fileName   = $tipsaShippingData['delivery_note'].'.pdf';

      $ioAdapter = $this->_file;

      if (!is_dir($pdfPath)) {
        $ioAdapter->mkdir($pdfPath, 0775);
      }

      $ioAdapter->open(array('path'=>$pdfPath));
      $ioAdapter->write($fileName, $pdfContent, 0666);

      $shipment->setShippingLabel(base64_decode((string) $tipsaShippingData['label']));
      $shipment->save();

      return true;
    }
}
