<?php
namespace Eadesigndev\Pdfgenerator\Controller\Adminhtml\Templates\Delete;

/**
 * Interceptor class for @see \Eadesigndev\Pdfgenerator\Controller\Adminhtml\Templates\Delete
 */
class Interceptor extends \Eadesigndev\Pdfgenerator\Controller\Adminhtml\Templates\Delete implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Framework\Registry $registry, \Eadesigndev\Pdfgenerator\Model\PdfgeneratorRepository $templateRepository)
    {
        $this->___init();
        parent::__construct($context, $resultPageFactory, $registry, $templateRepository);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
