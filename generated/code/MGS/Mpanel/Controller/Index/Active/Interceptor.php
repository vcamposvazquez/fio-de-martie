<?php
namespace MGS\Mpanel\Controller\Index\Active;

/**
 * Interceptor class for @see \MGS\Mpanel\Controller\Index\Active
 */
class Interceptor extends \MGS\Mpanel\Controller\Index\Active implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Element\Context $urlContext, \Magento\Customer\Model\Session $customerSession)
    {
        $this->___init();
        parent::__construct($context, $urlContext, $customerSession);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
