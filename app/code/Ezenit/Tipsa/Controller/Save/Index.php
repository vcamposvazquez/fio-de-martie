<?php
namespace Ezenit\Tipsa\Controller\Save;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * Show Sample Module main page
     *
     * @return void
     */
    protected $helper;

    public function __construct(
         \Magento\Framework\App\Action\Context $context,
         \Ezenit\Tipsa\Helper\Data $helper
     ) {
         parent::__construct($context);
         $this->helper = $helper;
    }
    public function execute()
    {
      $this->helper->addYupickOrder($_REQUEST['quote_id'],$_REQUEST['yupick_id'],$_REQUEST['name'],$_REQUEST['address'],$_REQUEST['postal_code'],$_REQUEST['city']);
      echo json_encode(array("success" => true));
    }
}
