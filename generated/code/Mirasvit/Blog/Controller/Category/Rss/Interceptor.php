<?php
namespace Mirasvit\Blog\Controller\Category\Rss;

/**
 * Interceptor class for @see \Mirasvit\Blog\Controller\Category\Rss
 */
class Interceptor extends \Mirasvit\Blog\Controller\Category\Rss implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Mirasvit\Blog\Model\CategoryFactory $authorFactory, \Magento\Framework\Registry $registry, \Magento\Framework\App\Action\Context $context)
    {
        $this->___init();
        parent::__construct($authorFactory, $registry, $context);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
