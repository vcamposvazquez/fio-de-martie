var gMapsLoaded = false;
window.gMapsCallback = function(){
    gMapsLoaded = true;
    jQuery(window).trigger('gMapsLoaded');
}
window.loadGoogleMaps = function(apiKey){
    if(gMapsLoaded) return null;
    gMapsLoaded = true;
    var script_tag = document.createElement('script');
    script_tag.setAttribute("type","text/javascript");
    script_tag.setAttribute("src","//maps.google.com/maps/api/js?key=" + apiKey + "&callback=gMapsCallback");
    (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
}
