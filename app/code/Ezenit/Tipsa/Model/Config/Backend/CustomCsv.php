<?php

namespace Ezenit\Tipsa\Model\Config\Backend;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\MediaStorage\Model\File\Uploader;

class CustomCsv extends \Magento\Config\Model\Config\Backend\File
{
    /**
     * @return string[]
     */
     protected $helper;
     public function __construct(
         \Magento\Framework\Model\Context $context,
         \Magento\Framework\Registry $registry,
         \Magento\Framework\App\Config\ScopeConfigInterface $config,
         \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
         \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory,
         \Magento\Config\Model\Config\Backend\File\RequestData\RequestDataInterface $requestData,
         Filesystem $filesystem,
         \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
         \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
         \Ezenit\Tipsa\Helper\Data $helper,
         array $data = []
     ) {
        $this->helper = $helper;
        parent::__construct($context, $registry, $config, $cacheTypeList,$uploaderFactory, $requestData,$filesystem,$resource, $resourceCollection, $data);
     }

     public function beforeSave()
     {
         $value = $this->getValue();
         $file  = $this->getFileData();
         if (!empty($file)) {
             $uploadDir = $this->_getUploadDir();
             try {
                 /** @var Uploader $uploader */
                 $uploader = $this->_uploaderFactory->create(['fileId' => $file]);
                 $uploader->setAllowedExtensions($this->_getAllowedExtensions());
                 $uploader->setAllowRenameFiles(true);
                 $uploader->addValidateCallback('size', $this, 'validateMaxSize');
                 $result = $uploader->save($uploadDir);
             } catch (\Exception $e) {
                 throw new \Magento\Framework\Exception\LocalizedException(__('%1', $e->getMessage()));
             }

             $filename = $result['file'];
             if ($filename) {
                 if ($this->_addWhetherScopeInfo()) {
                     $filename = $this->_prependScopeInfo($filename);
                 }
                 // added
                 try{
                  $this->helper->updateFromCsv($uploadDir . "/" . $filename);
                 }catch (\Exception $e) {
                     throw new \Magento\Framework\Exception\LocalizedException(__('%1', $e->getMessage()));
                 }
                 // end added
                 $this->setValue($filename);
             }
         } else {
             if (is_array($value) && !empty($value['delete'])) {
                 $this->setValue('');
             } else {
                 $this->unsValue();
             }
         }

         return $this;
     }


    public function getAllowedExtensions() {
        return ['xls'];
    }
}
