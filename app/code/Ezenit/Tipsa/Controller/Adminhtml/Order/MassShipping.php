<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Ezenit\Tipsa\Controller\Adminhtml\Order;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;

class MassShipping extends \Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction
{
  /**
   * @param Context $context
   * @param Filter $filter
   * @param CollectionFactory $collectionFactory
   */
  protected $logger;
  protected $helper;
  protected $shipmentFactory;

  public function __construct(
    Context $context,
    Filter $filter,
    CollectionFactory $collectionFactory,
    \Psr\Log\LoggerInterface $logger,
    \Ezenit\Tipsa\Helper\Data $helper,
    \Magento\Sales\Model\Convert\OrderFactory $convertOrderFactory,
    \Magento\Sales\Model\Order\ShipmentFactory $shipmentFactory
    )
  {
    parent::__construct($context, $filter);
    $this->collectionFactory = $collectionFactory;
    $this->logger            = $logger;
    $this->helper            = $helper;
    $this->converter         = $convertOrderFactory->create();
  }

  protected function massAction(AbstractCollection $collection)
  {
    $errorLog = [];
      foreach ($collection->getItems() as $order) {
        if (! $order->canShip()) {
          array_push($errorLog, "You cannot create a shipment for order".$order->getIncrementId());
        }
        $shipment   = $this->converter->toShipment($order);
        $orderItems = $order->getAllItems();
        foreach ($orderItems as $orderItem) {
          $qtyShipped = $orderItem->getQtyToShip();
          $shipmentItem = $this->converter->itemToShipmentItem($orderItem)->setQty($qtyShipped);
          $shipment->addItem($shipmentItem);
        }
        $shipment->register();
        $shipment->getOrder()->setIsInProcess(true);

        if (count($errorLog) > 0) {
          // $this->helper->logMessageToAdmin('error','There were errors generating the mass shipment');
          $resultRedirect = $this->resultRedirectFactory->create();
          $resultRedirect->setPath($this->getComponentRefererUrl());
          return $resultRedirect;
        }
        try {
          if ($shipment->save()) {
            $shipment->getOrder()->save();
          }
        } catch (Exception $e) {
          array_push($errorLog, $e->getMessage());
        }
      }
      $resultRedirect = $this->resultRedirectFactory->create();
      $resultRedirect->setPath($this->getComponentRefererUrl());
      return $resultRedirect;
    }
}
