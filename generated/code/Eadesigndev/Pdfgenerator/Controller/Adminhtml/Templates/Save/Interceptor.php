<?php
namespace Eadesigndev\Pdfgenerator\Controller\Adminhtml\Templates\Save;

/**
 * Interceptor class for @see \Eadesigndev\Pdfgenerator\Controller\Adminhtml\Templates\Save
 */
class Interceptor extends \Eadesigndev\Pdfgenerator\Controller\Adminhtml\Templates\Save implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Eadesigndev\Pdfgenerator\Controller\Adminhtml\Templates\PdfDataProcessor $dataProcessor, \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor, \Eadesigndev\Pdfgenerator\Model\PdfgeneratorRepository $templateRepository, \Eadesigndev\Pdfgenerator\Model\PdfgeneratorFactory $pdfgeneratorFactory)
    {
        $this->___init();
        parent::__construct($context, $dataProcessor, $dataPersistor, $templateRepository, $pdfgeneratorFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
