<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
</head>

<body>
<?php

  class generarEnvio{
    // Atributos
    public $tipsaCodigoAgencia = '000000';
    public $tipsaCodigoCliente = '33333';
    public $tipsaPasswordCliente = 'PR20@18';
    public $URL = "HTTP://webservices.tipsa-dinapaq.com:80/SOAP";

    // Métodos
    public function LoginCli($codigoAgencia,$codigoCliente,$passwordCliente,$url){
      $XML='<?xml version="1.0" encoding="utf-8"?>
        <soap:Envelope 
          xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" 
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
          xmlns:xsd="http://www.w3.org/2001/XMLSchema">
          <soap:Body>
            <LoginWSService___LoginCli>
              <strCodAge>'.$codigoAgencia.'</strCodAge>
              <strCod>'.$codigoCliente.'</strCod>
              <strPass>'.$passwordCliente.'</strPass>
            </LoginWSService___LoginCli>
          </soap:Body>
        </soap:Envelope>';
                  
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_HEADER, FALSE);
      curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
      curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
      curl_setopt($ch, CURLOPT_URL, $url );
      curl_setopt($ch, CURLOPT_POSTFIELDS, utf8_encode($XML) );
      curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
      $postResult = curl_exec($ch);      

      $dom = new DOMDocument;
      // load the XML string defined above
      $dom->loadXML($postResult);
      $note = $dom->getElementsByTagName("Envelope");  
      foreach( $note as $value ){
        $tasks = $value->getElementsByTagName("faultstring");
        if (isset($tasks)){
          @$task  = $tasks->item(0)->nodeValue;
        }
        $details = $value->getElementsByTagName("strSesion");
        @$idsesion  = $details->item(0)->nodeValue;
      }
      curl_close($ch);

      if($task){
        echo "<h1 style='color:red;'>".$task.' - ( Error de login )</h1>';
        die();
      }
      else{
        return $idsesion;
      }
    } // FIN public function login($codigoAgencia,$codigoCliente,$passwordCliente,$url){

    public function GrabaEnvio4($codigoAgencia,$codigoCliente,$url,$idSesionLogin){
      $XML='<?xml version="1.0" encoding="utf-8"?>
      <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
        <soap:Header>
          <ROClientIDHeader xmlns="http://tempuri.org/">
            <ID>'.$idSesionLogin.'</ID>
          </ROClientIDHeader>
        </soap:Header>
        <soap:Body>
          <WebServService___GrabaEnvio4 xmlns="http://tempuri.org/">
            <strCodAgeCargo>'.$codigoAgencia.'</strCodAgeCargo>
            <strCodAgeOri>'.$codigoAgencia.'</strCodAgeOri>
            <dtFecha>'.date("Y/m/d").'</dtFecha>
            <strCodTipoServ>14</strCodTipoServ>
            <strCodCli>'.$codigoCliente.'</strCodCli>
            <strNomOri>Nombre origen prueba</strNomOri>
            <strNomDes>Nombre destinatario prueba</strNomDes>
            <strDirDes>Direccion destinatario prueba</strDirDes>
            <strCPDes>28011</strCPDes>                        
            <strObs>Esto es una observacion de prueba</strObs>                 
            <intPaq>1</intPaq>
            <dPesoOri>1</dPesoOri>
            <boInsert>'.TRUE.'</boInsert>
          </WebServService___GrabaEnvio4>
        </soap:Body>
      </soap:Envelope>';
      
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_HEADER, FALSE);
      curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
      curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
      curl_setopt($ch, CURLOPT_URL, $url );
      curl_setopt($ch, CURLOPT_POSTFIELDS, utf8_encode($XML) );
      curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
      $postResult = curl_exec($ch); 
  
      $dom = new DOMDocument;
      // load the XML string defined above
      $dom->loadXML($postResult);
       
      $note = $dom->getElementsByTagName("Envelope");

      foreach( $note as $value ){
        $tasks = $value->getElementsByTagName("faultstring");
        if (isset($tasks)){
          @$task  = $tasks->item(0)->nodeValue;
        }
        $details = $value->getElementsByTagName("strAlbaranOut");
        @$albaran  = $details->item(0)->nodeValue;
      }
      curl_close($ch);
      
      if($task){
        echo "<h1 style='color:red;'>".$task.' - ( Error al general en envio )</h1>';
        die();
      }
      else{
        return $albaran;
      }
    } // public function grabaEnvio4($codigoAgencia,$codigoCliente,$url,$idSesionLogin){
  } // FIN class generarEnvio(){ 

  // creamos la clase generar envio
  $envio = new generarEnvio();

  // Obtenemos el id sesion con el método loginCli
  $idSesionLogin = $envio->LoginCli($envio->tipsaCodigoAgencia,$envio->tipsaCodigoCliente,$envio->tipsaPasswordCliente,$envio->URL);
  echo 'Id Sesion: '.$idSesionLogin."<br>";
  // Obtenemos el numero albaran del envio con el método GrabaEnvio4
  $numAlbaran = $envio->GrabaEnvio4($envio->tipsaCodigoAgencia,$envio->tipsaCodigoCliente,$envio->URL,$idSesionLogin);
  echo 'Numero de albaran: '.$numAlbaran."<br>";
?>
</body>
</html>