<?php
namespace MGS\AjaxCart\Block\Ui\Related;

/**
 * Interceptor class for @see \MGS\AjaxCart\Block\Ui\Related
 */
class Interceptor extends \MGS\AjaxCart\Block\Ui\Related implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Catalog\Model\ResourceModel\Product\Link\Product\CollectionFactory $collectionFactory, \Magento\Framework\Module\Manager $moduleManager, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Catalog\Block\Product\Context $context, \MGS\AjaxCart\Helper\Data $aHelper, \Magento\Framework\Data\Form\FormKey $formKey, \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus, \Magento\Catalog\Model\Product\Visibility $productVisibility, array $data = array())
    {
        $this->___init();
        parent::__construct($collectionFactory, $moduleManager, $checkoutSession, $context, $aHelper, $formKey, $productStatus, $productVisibility, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function getImage($product, $imageId, $attributes = array())
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getImage');
        if (!$pluginInfo) {
            return parent::getImage($product, $imageId, $attributes);
        } else {
            return $this->___callPlugins('getImage', func_get_args(), $pluginInfo);
        }
    }
}
