<?php

namespace Ezenit\Tipsa\Model\Source;

class Free
{
    public function toOptionArray()
    {
        $arr = array();
        $arr[] = array('value'=>'tipsa_49', 'label'=>'STANDARD');
        $arr[] = array('value'=>'tipsa_24', 'label'=>'PREMIUM');
    		$arr[] = array('value'=>'tipsa_50', 'label'=>'YUPICK');
    		$arr[] = array('value'=>'tipsa_57', 'label'=>'YUPICK BALEARES');
        return $arr;
    }

    public function getMethod($shippingMethod)
    {
      $mte= explode("_",$shippingMethod);
      if ($mte[0]=="tipsa")
      {
        $label=$mte[1];
        foreach($this->toOptionArray() as $mta)
        {
          if ($mta["label"]==$label) $method=$mta;
        }
      }
      return $method;
    }
}
?>
