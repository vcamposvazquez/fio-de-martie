<?php
namespace Ezenit\Tipsa\Controller\Spots\Index;

/**
 * Interceptor class for @see \Ezenit\Tipsa\Controller\Spots\Index
 */
class Interceptor extends \Ezenit\Tipsa\Controller\Spots\Index implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Ezenit\Tipsa\Helper\Data $helper)
    {
        $this->___init();
        parent::__construct($context, $inlineTranslation, $scopeConfig, $helper);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
