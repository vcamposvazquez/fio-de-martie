<?php
namespace MGS\Mpanel\Controller\Element\Save;

/**
 * Interceptor class for @see \MGS\Mpanel\Controller\Element\Save
 */
class Interceptor extends \MGS\Mpanel\Controller\Element\Save implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Customer\Model\Session $customerSession, \Magento\Framework\Filesystem $filesystem, \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory, \Magento\Framework\Filesystem\Driver\File $file, \Magento\Framework\View\Element\Context $urlContext)
    {
        $this->___init();
        parent::__construct($context, $storeManager, $customerSession, $filesystem, $fileUploaderFactory, $file, $urlContext);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
