<?php
namespace Ezenit\Tipsa\Logger;

class Handler extends \Magento\Framework\Logger\Handler\Base
{
    protected $loggerType = Logger::DEBUG;
    protected $fileName = '/var/log/tipsa/debug.log';
}
