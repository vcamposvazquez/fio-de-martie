<?php
return [
    'backend' => [
        'frontName' => 'adminfiodemartie'
    ],
    'crypt' => [
        'key' => '594722bb80d7675c119130dba8cc00f9'
    ],
    'db' => [
        'table_prefix' => 'fdm_',
        'connection' => [
            'default' => [
                'host' => 'localhost',
                'dbname' => 'wwwfiodemartie_main',
                'username' => 'wwwfiode_user',
                'password' => 'V.YZ(!sG0bX8',
                'active' => '1',
                'model' => 'mysql4',
                'engine' => 'innodb',
                'initStatements' => 'SET NAMES utf8;'
            ]
        ]
    ],
    'resource' => [
        'default_setup' => [
            'connection' => 'default'
        ]
    ],
    'x-frame-options' => 'SAMEORIGIN',
    'MAGE_MODE' => 'production',
    'session' => [
        'save' => 'files'
    ],
    'cache_types' => [
        'config' => 1,
        'layout' => 1,
        'block_html' => 1,
        'collections' => 1,
        'reflection' => 1,
        'db_ddl' => 1,
        'eav' => 1,
        'customer_notification' => 1,
        'config_integration' => 1,
        'config_integration_api' => 1,
        'full_page' => 1,
        'translate' => 1,
        'config_webservice' => 1,
        'compiled_config' => 1
    ],
    'install' => [
        'date' => 'Wed, 17 Jan 2018 19:44:24 +0000'
    ],
    'system' => [
        'default' => [
            'dev' => [
                'debug' => [
                    'debug_logging' => '0'
                ]
            ]
        ]
    ]
];
