<?php
namespace Mirasvit\Blog\Model\ResourceModel\Category;

/**
 * Interceptor class for @see \Mirasvit\Blog\Model\ResourceModel\Category
 */
class Interceptor extends \Mirasvit\Blog\Model\ResourceModel\Category implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Mirasvit\Blog\Model\Config $config, \Magento\Framework\Filter\FilterManager $filter, \Magento\Eav\Model\Entity\Context $context, $data = array())
    {
        $this->___init();
        parent::__construct($config, $filter, $context, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function save(\Magento\Framework\Model\AbstractModel $object)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'save');
        if (!$pluginInfo) {
            return parent::save($object);
        } else {
            return $this->___callPlugins('save', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function delete($object)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'delete');
        if (!$pluginInfo) {
            return parent::delete($object);
        } else {
            return $this->___callPlugins('delete', func_get_args(), $pluginInfo);
        }
    }
}
