<?php
namespace Magento\Variable\Model\Variable\Config;

/**
 * Interceptor class for @see \Magento\Variable\Model\Variable\Config
 */
class Interceptor extends \Magento\Variable\Model\Variable\Config implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\View\Asset\Repository $assetRepo, \Magento\Backend\Model\UrlInterface $url)
    {
        $this->___init();
        parent::__construct($assetRepo, $url);
    }

    /**
     * {@inheritdoc}
     */
    public function getVariablesWysiwygActionUrl()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getVariablesWysiwygActionUrl');
        if (!$pluginInfo) {
            return parent::getVariablesWysiwygActionUrl();
        } else {
            return $this->___callPlugins('getVariablesWysiwygActionUrl', func_get_args(), $pluginInfo);
        }
    }
}
