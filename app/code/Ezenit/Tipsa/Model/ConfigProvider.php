<?php

namespace Ezenit\Tipsa\Model;

use Magento\Checkout\Model\ConfigProviderInterface;

/**
 * Class SampleConfigProvider
 */
class ConfigProvider implements ConfigProviderInterface
{
    protected $helper;
    protected $cart_session;
    protected $store;
    protected $_assetRepo;

    public function __construct(
      \Ezenit\Tipsa\Helper\Data $helper,
      \Magento\Checkout\Model\Session $cartSession,
      \Magento\Store\Model\StoreManagerInterface $storeManager,
      \Magento\Framework\View\Asset\Repository $assetRepo
      )
    {
      $this->helper = $helper;
      $this->cart_session         = $cartSession;
      $this->store                = $storeManager->getStore();
      $this->_assetRepo           = $assetRepo;
    }

    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
    public function getConfig()
    {
        $apikey         = $this->helper->getApiKey();
        $logo           = $this->_assetRepo->getUrl("Ezenit_Tipsa::images/marker.png");
        // $yupickSpots    = $this->helper->getYupickSpots($postalCode);
        return [
            'ezenit' => [
                'ezMap'           => "err",
                'ezMarkers'       => [],
                'ezInfoWindows'   => [],
                'apikey'          => $apikey,
                'logo'            => $logo,
                'quote'           => $this->getQuote()->getId(),
                'urls'   => array(
                  'spots' => $this->getUrl('yupick/spots'),
                  'save'  => $this->getUrl('yupick/save'),
                  'delete'=> $this->getUrl('yupick/delete')
                )
                 // 'spots'  => $yupickSpots
            ],
        ];
    }
    public function getUrl($target)  { return $this->store->getUrl($target);}
    public function getQuote()       { return $this->cart_session->getQuote();}
}
