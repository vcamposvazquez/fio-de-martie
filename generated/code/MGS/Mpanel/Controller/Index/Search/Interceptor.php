<?php
namespace MGS\Mpanel\Controller\Index\Search;

/**
 * Interceptor class for @see \MGS\Mpanel\Controller\Index\Search
 */
class Interceptor extends \MGS\Mpanel\Controller\Index\Search implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Catalog\Block\Product\Context $catalogContext, \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory, \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility)
    {
        $this->___init();
        parent::__construct($context, $catalogContext, $productCollectionFactory, $catalogProductVisibility);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
