<?php
namespace Eadesigndev\Pdfgenerator\Controller\Adminhtml\Variable\Template;

/**
 * Interceptor class for @see \Eadesigndev\Pdfgenerator\Controller\Adminhtml\Variable\Template
 */
class Interceptor extends \Eadesigndev\Pdfgenerator\Controller\Adminhtml\Variable\Template implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Registry $coreRegistry, \Magento\Email\Model\Template\Config $emailConfig, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \Magento\Framework\Json\Helper\Data $jsonHelperData, \Magento\Variable\Model\Variable $variableModel, \Magento\Email\Model\Source\Variables $emailVariables, \Magento\Email\Model\BackendTemplate $emailBackendTemplate)
    {
        $this->___init();
        parent::__construct($context, $coreRegistry, $emailConfig, $resultJsonFactory, $jsonHelperData, $variableModel, $emailVariables, $emailBackendTemplate);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
