<?php
namespace Ezenit\Tipsa\Model\Sales;

class Plugin
{
  public function beforeSetTemplateVars($subject,$vars)
  {
      /** @var Order $order */
      $order = $vars['order'];
      $method = $order->getShippingMethod();

      $vars['ez_tipsa']           = $method === 'tipsa_tipsa';
      $vars['ez_shipping_method'] = $method;

      return [$vars];
  }
}
