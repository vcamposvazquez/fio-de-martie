<?php
namespace Yireo\CheckoutTester2\Controller\Index\Success;

/**
 * Interceptor class for @see \Yireo\CheckoutTester2\Controller\Index\Success
 */
class Interceptor extends \Yireo\CheckoutTester2\Controller\Index\Success implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Framework\Registry $registry, \Magento\Checkout\Model\Session $checkoutSession, \Yireo\CheckoutTester2\Helper\Data $moduleHelper, \Yireo\CheckoutTester2\Helper\Order $orderHelper)
    {
        $this->___init();
        parent::__construct($context, $resultPageFactory, $registry, $checkoutSession, $moduleHelper, $orderHelper);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
