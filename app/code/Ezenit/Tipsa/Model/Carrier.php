<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Ezenit\Tipsa\Model;

use Magento\Quote\Model\Quote\Address\RateResult\Error;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Carrier\AbstractCarrierOnline;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Rate\Result;
use Magento\Shipping\Model\Simplexml\Element;
use Magento\Framework\Xml\Security;
use Magento\Catalog\Model\Product\Type;
use Magento\Framework\Module\Dir;
use Magento\Sales\Model\Order\Shipment;

class Carrier extends AbstractCarrierOnline implements CarrierInterface
{
  /**
   * @var string
   */
  protected $_code = 'tipsa';
  protected $_request = null;
  protected $_result = null;

  /**
   * @var bool
   */
  protected $_isFixed = true;
  protected $string;

  /**
   * @var \Magento\Shipping\Model\Rate\ResultFactory
   */
  protected $_rateResultFactory;
  protected $_maxWeight = 70;

  protected $_freeMethod = 'free_method';

  const CONFIG_FREE_SHIPPING_METHOD_PATH = 'ezenit_tipsa_settings/ez_tipsa_free_shipping_options/tipsa_free_shipping_service';

  /**
   * @var \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory
   */
  protected $_rateMethodFactory;

  /**
   * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
   * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
   * @param \Psr\Log\LoggerInterface $logger
   * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
   * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
   * @param array $data
   */
  protected $helper;
  protected $orderItemRepository;

  public function __construct(
    // \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
    \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
    // \Psr\Log\LoggerInterface $logger,
    Security $xmlSecurity,
    \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
    \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
    \Magento\Framework\App\Helper\Context $context,
    \Magento\Shipping\Model\Simplexml\ElementFactory $xmlElFactory,
    \Magento\Shipping\Model\Rate\ResultFactory $rateFactory,
    \Magento\Shipping\Model\Tracking\ResultFactory $trackFactory,
    \Magento\Shipping\Model\Tracking\Result\ErrorFactory $trackErrorFactory,
    \Magento\Shipping\Model\Tracking\Result\StatusFactory $trackStatusFactory,
    \Magento\Directory\Model\RegionFactory $regionFactory,
    \Magento\Directory\Model\CountryFactory $countryFactory,
    \Magento\Directory\Model\CurrencyFactory $currencyFactory,
    \Magento\Directory\Helper\Data $directoryData,
    \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
    \Magento\Framework\Locale\FormatInterface $localeFormat,
    \Ezenit\Tipsa\Helper\Data $helper,
    \Magento\Sales\Model\Order\ItemRepository $orderItemRepository,
    \Magento\Framework\Stdlib\StringUtils $string,
    array $data = []
  ) {
      $this->helper               = $helper;
      $this->_rateResultFactory   = $rateResultFactory;
      $this->_rateMethodFactory   = $rateMethodFactory;
      $this->_orderItemRepository = $orderItemRepository;
      $this->string               = $string;
      parent::__construct(
        $context->getScopeConfig(),
        $rateErrorFactory,
        $context->getLogger(),
        $xmlSecurity,
        $xmlElFactory,
        $rateFactory,
        $rateMethodFactory,
        $trackFactory,
        $trackErrorFactory,
        $trackStatusFactory,
        $regionFactory,
        $countryFactory,
        $currencyFactory,
        $directoryData,
        $stockRegistry,
        $data
      );
      $this->helper->tipsaLogger("CONSTRUCTOR LOADED");
  }

  public function requestToShipment($request)
  {
    $packages = $request->getPackages();

    if (!is_array($packages) || !$packages) {
        $this->helper->tipsaLogger("No packages for request");
    }

    $request->setTotalPackages(count($packages));
    $result = $this->_doShipmentRequest($request);

    if ($result === false) {
      $this->helper->tipsaLogger("Could not do shipment request for current order");
      return false;
    }
    $urlKey = substr($result['tipsa_tracking_number'],1,-1);

    $url = 'https://dinapaqweb.tipsa-dinapaq.com/dinapaqweb/detalle_envio_sc.php?servicio='.$urlKey.'&fecha='.date("d/m/Y");

    $response = new \Magento\Framework\DataObject(
        [
            'info' => [
                [
                    'tracking_number' => $url,
                    'label_content' => base64_decode($result['label']),
                ],
            ],
        ]
    );

    $request->setMasterTrackingId($result['delivery_note']);
    return $response;
  }


  protected function _doShipmentRequest(\Magento\Framework\DataObject $request)
  {
    $packages    = $request->getTotalPackages();
    $myId        = json_encode(array_keys($request->getPackages()["1"]["items"])[0]);
    $orderItem   = $this->_orderItemRepository->get($myId);
    $orderId     = $orderItem->getOrderId();
    $order       = $this->helper->Order($orderId);
    $payment     = $order->getPayment();
    $tracks      = [];
    $shipment    = [];
    $address     = $order->getShippingAddress();
    $result      = $this->helper->setShipment($shipment,$order,$payment,$tracks,$address,$packages);
    if ($result === false) {
      $this->helper->tipsaLogger("THE SHIPMENT COULDN'T BE CREATED");
      return false;
    }
    return $result;
  }

  public function getResult()
  {
      return $this->_result;
  }

  protected function _getDefaultValue($origValue, $pathToValue)
  {
      if (!$origValue) {
          $origValue = $this->_scopeConfig->getValue(
              $pathToValue,
              \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
              $this->getStore()
          );
      }

      return $origValue;
  }

  public function setRequest(\Magento\Framework\DataObject $request)
  {
      $this->_request = $request;

      $this->setStore($request->getStoreId());

      $requestObject = new \Magento\Framework\DataObject();

      $requestObject->setIsGenerateLabelReturn($request->getIsGenerateLabelReturn());

      $requestObject->setStoreId($request->getStoreId());

      if ($request->getLimitMethod()) {
          $requestObject->setService($request->getLimitMethod());
      }

      // $requestObject = $this->_addParams($requestObject);

      if ($request->getDestPostcode()) {
          $requestObject->setDestPostal($request->getDestPostcode());
      }

      $requestObject->setOrigCountry(
          $this->_getDefaultValue($request->getOrigCountry(), Shipment::XML_PATH_STORE_COUNTRY_ID)
      )->setOrigCountryId(
          $this->_getDefaultValue($request->getOrigCountryId(), Shipment::XML_PATH_STORE_COUNTRY_ID)
      );

      $shippingWeight = $request->getPackageWeight();

      $requestObject->setValue(round($request->getPackageValue(), 2))
          ->setValueWithDiscount($request->getPackageValueWithDiscount())
          ->setCustomsValue($request->getPackageCustomsValue())
          ->setDestStreet($this->string->substr(str_replace("\n", '', $request->getDestStreet()), 0, 35))
          ->setDestStreetLine2($request->getDestStreetLine2())
          ->setDestCity($request->getDestCity())
          ->setOrigCompanyName($request->getOrigCompanyName())
          ->setOrigCity($request->getOrigCity())
          ->setOrigPhoneNumber($request->getOrigPhoneNumber())
          ->setOrigPersonName($request->getOrigPersonName())
          ->setOrigEmail(
              $this->_scopeConfig->getValue(
                  'trans_email/ident_general/email',
                  \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                  $requestObject->getStoreId()
              )
          )
          ->setOrigCity($request->getOrigCity())
          ->setOrigPostal($request->getOrigPostal())
          ->setOrigStreetLine2($request->getOrigStreetLine2())
          ->setDestPhoneNumber($request->getDestPhoneNumber())
          ->setDestPersonName($request->getDestPersonName())
          ->setDestCompanyName($request->getDestCompanyName());

      $originStreet2 = $this->_scopeConfig->getValue(
          Shipment::XML_PATH_STORE_ADDRESS2,
          \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
          $requestObject->getStoreId()
      );

      $requestObject->setOrigStreet($request->getOrigStreet() ? $request->getOrigStreet() : $originStreet2);

      if (is_numeric($request->getOrigState())) {
          $requestObject->setOrigState($this->_regionFactory->create()->load($request->getOrigState())->getCode());
      } else {
          $requestObject->setOrigState($request->getOrigState());
      }

      $destCountry = $request->getDestCountryId();

      if ($request->getPackageId()) {
          $requestObject->setPackageId($request->getPackageId());
      }

      $requestObject->setBaseSubtotalInclTax($request->getBaseSubtotalInclTax());

      $this->setRawRequest($requestObject);

      return $this;
  }

  // $this->_request = $request;
  // $this->setRawRequest($this->_request);

  public function collectRates(RateRequest $request)
  {
    $this->setRequest($request);

    $this->_getQuotes();

    $this->_updateFreeMethodQuote($request);

    return $this->getResult();
  }

  public function _getQuotes()
  {
    if (!$this->getConfigFlag('active')) {
        return false;
    }

    $this->_result = $this->_rateResultFactory->create();
    $shipment                   = [];
    $ezRequest                  = $this->_request->getData();
    $shipment['packageQty']     = $this->_request->getPackageQty();
    $shipment['subTotal']       = $ezRequest['package_physical_value'];
    $shipment['price']          = $ezRequest['base_subtotal_incl_tax'];
    $shipment['weight']         = $this->_request->getPackageWeight();
    $shipment['destCountry']    = $ezRequest['dest_country_id'];
    $shipment['destPostalCode'] = $ezRequest['dest_postcode'];
    $shipment['tax']            = round((floatval($shipment['price']) / floatval($shipment['subTotal'])),2 );

    $freeShippingEnabled = $this->_scopeConfig->getValue('ezenit_tipsa_settings/ez_tipsa_free_shipping_options/tipsa_free_shipping_active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) == 1 ? true : false;

    $freeShippingService = "";

    if ($freeShippingEnabled == true) {
      $freeShippingMinPrice = $this->_scopeConfig->getValue('ezenit_tipsa_settings/ez_tipsa_free_shipping_options/tipsa_free_shipping_min', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
      if ($shipment['price'] >= $freeShippingMinPrice) {
        $freeShippingService = $this->_scopeConfig->getValue('ezenit_tipsa_settings/ez_tipsa_free_shipping_options/tipsa_free_shipping_service', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $freeShippingServiceCode = str_replace("tipsa_","",$freeShippingService);
      } else {
        $freeShippingService = "none";
      }
    } else {
      $freeShippingService = "none";
    }

    $enabledServices = [];
    $enabledServices = array(
      'tipsa_49' => $this->_scopeConfig->getValue('ezenit_tipsa_settings/ez_tipsa_shipping_service_tipsa_49/tipsa_shipping_service_49_active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) == 1 ? true : false,
      'tipsa_24' => $this->_scopeConfig->getValue('ezenit_tipsa_settings/ez_tipsa_shipping_service_tipsa_24/tipsa_shipping_service_24_active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) == 1 ? true : false,
      'tipsa_50' => $this->_scopeConfig->getValue('ezenit_tipsa_settings/ez_tipsa_shipping_service_tipsa_50/tipsa_shipping_service_50_active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) == 1 ? true : false,
      'tipsa_57' => $this->_scopeConfig->getValue('ezenit_tipsa_settings/ez_tipsa_shipping_service_tipsa_57/tipsa_shipping_service_57_active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) == 1 ? true : false
    );

    $avaliableServices = $this->helper->getFares($shipment['destCountry'],$shipment['destPostalCode'],$shipment['weight'],$enabledServices);

    if ($avaliableServices != false) {
      foreach ($avaliableServices as $service) {
        $_service       = strtolower($service['service']);
        $_price         = $service['shipping_price'];
        $_increment     = $service['increment'];
        $_description   = $this->_scopeConfig->getValue('ezenit_tipsa_settings/ez_tipsa_shipping_service_tipsa_' . $_service . '/tipsa_service_description', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $_fixedPrice    = $this->_scopeConfig->getValue('ezenit_tipsa_settings/ez_tipsa_shipping_service_tipsa_' . $_service . '/tipsa_fixed_price', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $_manipulation  = $this->_scopeConfig->getValue('ezenit_tipsa_settings/ez_tipsa_shipping_service_tipsa_' . $_service . '/tipsa_manipulation_type', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $_fixedAmount   = $this->_scopeConfig->getValue('ezenit_tipsa_settings/ez_tipsa_shipping_service_tipsa_' . $_service . '/tipsa_fixed_amount', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        // $_smsActive     = $this->_scopeConfig->getValue('ezenit_tipsa_settings/ez_tipsa_shipping_service_tipsa_' . $_service . '/tipsa_sms', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $method = $this->_rateMethodFactory->create();
        $method->setCarrier($this->getCarrierCode());
        $method->setCarrierTitle($this->getConfigData('title'));
        $method->setMethod("tipsa_".$_service);

        if ($_increment) {
          $extraWeight = $shipment['weight'] - 10;
          $_price = $_price + ($extraWeight * $_increment);
        }

        if (($_manipulation == 'V' || $_manipulation == 'F') && !is_numeric($_fixedAmount) ) {
          $this->helper->tipsaLogger("ERR TIPSA - NO FIXED AMOUNT.");
          return false;
        }

        if ($_manipulation == 'V') {
          $rate   = $_fixedAmount/100 + 1;
          $_price = $_price * $rate;
        } else if ($_manipulation == 'F') {
          $_price = $_price + $_fixedAmount;
        }

        if (is_numeric($_fixedPrice) && $_fixedPrice != 0) {
          $_price = $_fixedPrice;
        }

        if ($freeShippingService != "none" && $_service == $freeShippingServiceCode) {
          $_price = 0.00;
          $_description = $_description . " - " . __("Free Shipping Offer");
        }

        if ($this->_request->getFreeShipping() === true) {
          $_price = 0;
        }

        $method->setMethodTitle($_description);
        $method->setPrice($this->getFinalPriceWithHandlingFee($_price));
        $this->_result->append($method);
      }
    }

    return $this->_result;
  }

  protected function _setFreeMethodRequest($freeMethod)
  {
    $this->_request->setFreeMethodRequest(true);
    $freeWeight = $this->getTotalNumOfBoxes($this->_rawRequest->getFreeMethodWeight());
    $this->_rawRequest->setWeight($freeWeight);
    $this->_rawRequest->setService($freeMethod);
  }

  public function getAllowedMethods()
  {
    return [$this->getCarrierCode() => __($this->getConfigData('name'))];
  }
}
