<?php
namespace Ezenit\Tipsa\Setup;
use Magento\Framework\Module\Setup\Migration;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
class InstallData implements InstallDataInterface
{

    public function __construct(
      \Ezenit\Tipsa\Helper\Data $helper
      )
    {
        $this->helper = $helper;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
      
    $installer     = $setup;
    $installer->startSetup();

    $path = str_replace("Setup","import",realpath(dirname(__FILE__)));

    $this->helper->updateFromCsv($path.'/tipsa.tarifas.csv');

    $installer->endSetup();
}
}
