<?php
namespace Eadesigndev\Pdfgenerator\Controller\Adminhtml\Order\Invoice\Printpdf;

/**
 * Interceptor class for @see \Eadesigndev\Pdfgenerator\Controller\Adminhtml\Order\Invoice\Printpdf
 */
class Interceptor extends \Eadesigndev\Pdfgenerator\Controller\Adminhtml\Order\Invoice\Printpdf implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Registry $coreRegistry, \Magento\Email\Model\Template\Config $emailConfig, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \Eadesigndev\Pdfgenerator\Helper\Pdf $helper, \Magento\Framework\Stdlib\DateTime\DateTime $dateTime, \Magento\Framework\App\Response\Http\FileFactory $fileFactory, \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory, \Eadesigndev\Pdfgenerator\Model\PdfgeneratorRepository $pdfGeneratorRepository, \Magento\Sales\Model\Order\InvoiceRepository $invoiceRepository)
    {
        $this->___init();
        parent::__construct($context, $coreRegistry, $emailConfig, $resultJsonFactory, $helper, $dateTime, $fileFactory, $resultForwardFactory, $pdfGeneratorRepository, $invoiceRepository);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
